<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="2.14.3-Essen" minimumScale="0" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <pipe>
    <rasterrenderer opacity="1" alphaBand="0" classificationMax="3788" classificationMinMaxOrigin="CumulativeCutFullExtentEstimated" band="1" classificationMin="269" type="singlebandpseudocolor">
      <rasterTransparency/>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" clip="0">
          <item alpha="255" value="269" label="269.000000" color="#54e597"/>
          <item alpha="255" value="342.547" label="342.547100" color="#61f082"/>
          <item alpha="255" value="416.094" label="416.094200" color="#73f775"/>
          <item alpha="255" value="489.289" label="489.289400" color="#94fe85"/>
          <item alpha="255" value="562.837" label="562.836500" color="#9efe87"/>
          <item alpha="255" value="636.384" label="636.383600" color="#adfd88"/>
          <item alpha="255" value="709.931" label="709.930700" color="#b4fe8b"/>
          <item alpha="255" value="783.478" label="783.477800" color="#bdfe8c"/>
          <item alpha="255" value="857.025" label="857.024900" color="#c5fd8d"/>
          <item alpha="255" value="930.22" label="930.220100" color="#cffe90"/>
          <item alpha="255" value="1003.77" label="1003.767200" color="#d7fe92"/>
          <item alpha="255" value="1077.31" label="1077.314300" color="#dffe93"/>
          <item alpha="255" value="1150.86" label="1150.861400" color="#e7fe95"/>
          <item alpha="255" value="1224.41" label="1224.408500" color="#eefd97"/>
          <item alpha="255" value="1297.6" label="1297.603700" color="#f7fe9a"/>
          <item alpha="255" value="1371.15" label="1371.150800" color="#fefe9b"/>
          <item alpha="255" value="1444.7" label="1444.697900" color="#fdfc97"/>
          <item alpha="255" value="1518.24" label="1518.245000" color="#fef895"/>
          <item alpha="255" value="1591.79" label="1591.792100" color="#fdf292"/>
          <item alpha="255" value="1665.34" label="1665.339200" color="#feee92"/>
          <item alpha="255" value="1738.53" label="1738.534400" color="#fde98c"/>
          <item alpha="255" value="1812.08" label="1812.081500" color="#fee187"/>
          <item alpha="255" value="1885.63" label="1885.628600" color="#fddb7f"/>
          <item alpha="255" value="1959.18" label="1959.175700" color="#fed779"/>
          <item alpha="255" value="2032.72" label="2032.722800" color="#fdce7d"/>
          <item alpha="255" value="2105.92" label="2105.918000" color="#facb80"/>
          <item alpha="255" value="2179.47" label="2179.465100" color="#f8c583"/>
          <item alpha="255" value="2253.01" label="2253.012200" color="#f6c586"/>
          <item alpha="255" value="2326.56" label="2326.559300" color="#f4c588"/>
          <item alpha="255" value="2400.11" label="2400.106400" color="#f2be8b"/>
          <item alpha="255" value="2473.65" label="2473.653500" color="#f0bc8f"/>
          <item alpha="255" value="2546.85" label="2546.848700" color="#eebb91"/>
          <item alpha="255" value="2620.4" label="2620.395800" color="#ebbc99"/>
          <item alpha="255" value="2693.94" label="2693.942900" color="#ebc2a4"/>
          <item alpha="255" value="2767.49" label="2767.490000" color="#e8c5ac"/>
          <item alpha="255" value="2841.04" label="2841.037100" color="#e6c8b1"/>
          <item alpha="255" value="2914.23" label="2914.232300" color="#dfc6b3"/>
          <item alpha="255" value="2987.78" label="2987.779400" color="#ddc7b7"/>
          <item alpha="255" value="3061.33" label="3061.326500" color="#e0cfc2"/>
          <item alpha="255" value="3134.87" label="3134.873600" color="#e4d6cb"/>
          <item alpha="255" value="3208.42" label="3208.420700" color="#e8ddd4"/>
          <item alpha="255" value="3281.62" label="3281.615900" color="#ebe2db"/>
          <item alpha="255" value="3355.16" label="3355.163000" color="#efe7e1"/>
          <item alpha="255" value="3428.71" label="3428.710100" color="#f3eeea"/>
          <item alpha="255" value="3502.26" label="3502.257200" color="#f6f0ec"/>
          <item alpha="255" value="3575.8" label="3575.804300" color="#faf9f8"/>
          <item alpha="255" value="3649.35" label="3649.351400" color="#ffffff"/>
          <item alpha="255" value="3788" label="3788.000000" color="#ffffff"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeBlue="128" grayscaleMode="0" saturation="0" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2" zoomedInResampler="bilinear"/>
  </pipe>
  <blendMode>6</blendMode>
</qgis>
