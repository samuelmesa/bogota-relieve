<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="2.14.3-Essen" minimumScale="0" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <pipe>
    <rasterrenderer opacity="1" alphaBand="0" classificationMax="3788" classificationMinMaxOrigin="CumulativeCutFullExtentEstimated" band="1" classificationMin="269" type="singlebandpseudocolor">
      <rasterTransparency/>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" clip="0">
          <item alpha="255" value="269" label="269.000000" color="#55e598"/>
          <item alpha="255" value="342.547" label="342.547100" color="#5be498"/>
          <item alpha="255" value="416.094" label="416.094200" color="#65e19a"/>
          <item alpha="255" value="489.289" label="489.289400" color="#6fdf9d"/>
          <item alpha="255" value="562.837" label="562.836500" color="#56e597"/>
          <item alpha="255" value="636.384" label="636.383600" color="#58e691"/>
          <item alpha="255" value="709.931" label="709.930700" color="#5ae78e"/>
          <item alpha="255" value="783.478" label="783.477800" color="#5de888"/>
          <item alpha="255" value="857.025" label="857.024900" color="#5ee885"/>
          <item alpha="255" value="930.22" label="930.220100" color="#61e981"/>
          <item alpha="255" value="1003.77" label="1003.767200" color="#63ea7d"/>
          <item alpha="255" value="1077.31" label="1077.314300" color="#66eb79"/>
          <item alpha="255" value="1150.86" label="1150.861400" color="#68eb75"/>
          <item alpha="255" value="1224.41" label="1224.408500" color="#6bec72"/>
          <item alpha="255" value="1297.6" label="1297.603700" color="#6ded6e"/>
          <item alpha="255" value="1371.15" label="1371.150800" color="#77ee70"/>
          <item alpha="255" value="1444.7" label="1444.697900" color="#7bee72"/>
          <item alpha="255" value="1518.24" label="1518.245000" color="#83ef74"/>
          <item alpha="255" value="1591.79" label="1591.792100" color="#8af076"/>
          <item alpha="255" value="1665.34" label="1665.339200" color="#93f179"/>
          <item alpha="255" value="1738.53" label="1738.534400" color="#9af27c"/>
          <item alpha="255" value="1812.08" label="1812.081500" color="#a2f27f"/>
          <item alpha="255" value="1885.63" label="1885.628600" color="#a8f381"/>
          <item alpha="255" value="1959.18" label="1959.175700" color="#aff483"/>
          <item alpha="255" value="2032.72" label="2032.722800" color="#b7f586"/>
          <item alpha="255" value="2105.92" label="2105.918000" color="#bdf589"/>
          <item alpha="255" value="2179.47" label="2179.465100" color="#c3f68b"/>
          <item alpha="255" value="2253.01" label="2253.012200" color="#caf78e"/>
          <item alpha="255" value="2326.56" label="2326.559300" color="#d0f891"/>
          <item alpha="255" value="2400.11" label="2400.106400" color="#d8f994"/>
          <item alpha="255" value="2473.65" label="2473.653500" color="#e1f99a"/>
          <item alpha="255" value="2546.85" label="2546.848700" color="#e6fa9d"/>
          <item alpha="255" value="2620.4" label="2620.395800" color="#ecfaa1"/>
          <item alpha="255" value="2693.94" label="2693.942900" color="#f2faa6"/>
          <item alpha="255" value="2767.49" label="2767.490000" color="#f6fbab"/>
          <item alpha="255" value="2841.04" label="2841.037100" color="#fafaaf"/>
          <item alpha="255" value="2914.23" label="2914.232300" color="#fbf7b3"/>
          <item alpha="255" value="2987.78" label="2987.779400" color="#fbf3b7"/>
          <item alpha="255" value="3061.33" label="3061.326500" color="#fbf1ba"/>
          <item alpha="255" value="3134.87" label="3134.873600" color="#fceec0"/>
          <item alpha="255" value="3208.42" label="3208.420700" color="#fcedc4"/>
          <item alpha="255" value="3281.62" label="3281.615900" color="#fcebc8"/>
          <item alpha="255" value="3355.16" label="3355.163000" color="#e5d0c0"/>
          <item alpha="255" value="3428.71" label="3428.710100" color="#ebddd2"/>
          <item alpha="255" value="3502.26" label="3502.257200" color="#f2e7de"/>
          <item alpha="255" value="3575.8" label="3575.804300" color="#fdebe0"/>
          <item alpha="255" value="3649.35" label="3649.351400" color="#f8f3ef"/>
          <item alpha="255" value="3788" label="3788.000000" color="#ffffff"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="-60" contrast="-10"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeBlue="128" grayscaleMode="0" saturation="0" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2" zoomedInResampler="bilinear"/>
  </pipe>
  <blendMode>9</blendMode>
</qgis>
